const ProgramCategory = require("../models/ProgramCategory.model");

exports.get = (request, response) => {

    ProgramCategory.get((err, data) => {
        if(err)
            response.status(err.code).send(err.description);
        else
            response.status(data.code).send(data.description);
    });
}
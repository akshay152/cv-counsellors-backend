const Misc = require("../models/Misc.model");
const generalHelper = require("../helpers/GeneralHelper");

exports.submitReport = (request, response) => {
    if (!request.body || generalHelper.isEmptyObject(request.body)) {
        response.status(400).send({
            description: "Invalid request format!"
        });
        return;
    }

    if (!request.body.counsellor_id || request.body.counsellor_id === "" || !request.body.customer_id || request.body.customer_id === "" || !request.body.reason || request.body.reason === "") {
        return response.status(400).send({ description: "The request was found to have an invalid format." });
    }

    const customer_id = generalHelper.mysql_real_escape_string(request.body.customer_id);
    const counsellor_id = generalHelper.mysql_real_escape_string(request.body.counsellor_id);
    const reason = generalHelper.mysql_real_escape_string(request.body.reason);
    const description = generalHelper.mysql_real_escape_string(request.body.description);

    if (parseInt(customer_id) === request.user.id) {
        Misc.submitReport(customer_id, counsellor_id, reason, description, (err, data) => {
            if (err)
                response.status(err.code).send(err.description);
            else
                response.status(data.code).send(data);
        });
    }
    else {
        return response.status(403).send({ description: "You are not authorized to perform this action" });
    }
}

exports.buttonClickMail = (request, response) => {
    if (!request.body || generalHelper.isEmptyObject(request.body)) {
        response.status(400).send({
            description: "Invalid request format!"
        });
        return;
    }

    if (!request.body.name || request.body.name === "" || !request.body.mobile || request.body.mobile === "") {
        return response.status(400).send({ description: "The request was found to have an invalid format." });
    }

    const userName = generalHelper.mysql_real_escape_string(request.body.name);
    const userNumber = generalHelper.mysql_real_escape_string(request.body.mobile);
    const userMail = generalHelper.mysql_real_escape_string(request.body.email);
    const cvID = generalHelper.mysql_real_escape_string(request.body.cv_id);
    const userCourseID = request.body.course_id;
    const userSpecializationID = request.body.specialization_id;

    Misc.buttonClickMail(userName, userNumber, userMail, cvID, userCourseID, userSpecializationID, (err, data) => {
        if (err)
            response.status(err.code).send(err.description);
        else
            response.status(data.code).send(data);
    });
}
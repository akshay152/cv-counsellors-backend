const Review = require("../models/Review.model");
const generalHelper = require("../helpers/GeneralHelper");

exports.getForCounsellor = (request, response) => {

    if(!request.params || generalHelper.isEmptyObject(request.params)) {
        response.status(400).send({
            description: "Invalid request format!"
        });
        return;
    }

    if(!request.params.counsellor_slug || request.params.counsellor_slug === "") {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    const counsellor_slug = generalHelper.mysql_real_escape_string(request.params.counsellor_slug);

    Review.getForCounsellor(counsellor_slug, (err, data) => {
        if(err)
            response.status(err.code).send(err.description);
        else
            response.status(data.code).send(data.description);
    });
}
const Course = require("../models/Course.model");
const generalHelper = require("../helpers/GeneralHelper");

exports.get = (request, response) => {
    Course.get((err, data) => {
        if(err)
            response.status(err.code).send(err.description);
        else
            response.status(data.code).send(data.description);
    });
}

exports.getForConsultation = (request, response) => {
    Course.getForConsultation((err, data) => {
        if(err)
            response.status(err.code).send(err.description);
        else
            response.status(data.code).send(data.description);
    });
}

exports.getDetails = (request, response) => {
    if(!request.params || generalHelper.isEmptyObject(request.params)) {
        response.status(400).send({
            description: "Invalid request format!"
        });
        return;
    }

    if(!request.params.course_id || request.params.course_id === "") {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    const course_id = generalHelper.mysql_real_escape_string(request.params.course_id);

    Course.getDetails(course_id, (err, data) => {
        if(err)
            response.status(err.code).send(err.description);
        else
            response.status(data.code).send(data.description);
    });
}
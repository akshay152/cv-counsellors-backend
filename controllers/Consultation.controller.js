const Consultation = require("../models/Consultation.model");
const generalHelper = require("../helpers/GeneralHelper");

exports.create = (request, response) => {
    if(!request.body || generalHelper.isEmptyObject(request.body)) {
        response.status(400).send({
            description: "Invalid request format!"
        });
        return;
    }

    if(!request.body.counsellor_id || request.body.counsellor_id === "" || !request.body.customer_id || request.body.customer_id === "" || !request.body.specialization_id || request.body.specialization_id === "" || !request.body.start_time || request.body.start_time === "" || !request.body.transaction_id || request.body.transaction_id === "") {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    const counsellor_id = generalHelper.mysql_real_escape_string(request.body.counsellor_id);
    const customer_id = generalHelper.mysql_real_escape_string(request.body.customer_id);
    const specialization_id = generalHelper.mysql_real_escape_string(request.body.specialization_id);
    const start_time = generalHelper.mysql_real_escape_string(request.body.start_time);
    const transaction_id = generalHelper.mysql_real_escape_string(request.body.transaction_id);

    if(parseInt(customer_id) === request.user.id) {
        Consultation.create(counsellor_id, customer_id, specialization_id, start_time, transaction_id, (err, data) => {
            if(err)
                response.status(err.code).send(err.description);
            else
                response.status(data.code).send(data);
        });
    }
    else {
        return response.status(403).send({description: "You are not authorized to perform this action"});
    }
}

exports.getForACustomer = (request, response) => {
    if(!request.params || generalHelper.isEmptyObject(request.params)) {
        response.status(400).send({
            description: "Invalid request format!"
        });
        return;
    }

    if(!request.params.customer_id || request.params.customer_id === "") {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    const customer_id = generalHelper.mysql_real_escape_string(request.params.customer_id);

    if(parseInt(customer_id) === request.user.id) {
        Consultation.getForACustomer(customer_id, (err, data) => {
            if(err)
                response.status(err.code).send(err.description);
            else
                response.status(data.code).send(data);
        });
    }
    else {
        return response.status(403).send({description: "You are not authorized to perform this action"});
    }
}

exports.getMessages = (request, response) => {
    if(!request.params || generalHelper.isEmptyObject(request.params) || !request.query || generalHelper.isEmptyObject(request.query)) {
        response.status(400).send({
            description: "Invalid request format!"
        });
        return;
    }

    if(!request.params.customer_id || request.params.customer_id === "" || !request.params.consultation_id || request.params.consultation_id === "" || !request.query.lower_limit || request.query.lower_limit === "" || !request.query.upper_limit || request.query.upper_limit === "") {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    const customer_id = generalHelper.mysql_real_escape_string(request.params.customer_id);
    const consultation_id = generalHelper.mysql_real_escape_string(request.params.consultation_id);
    const lower_limit = generalHelper.mysql_real_escape_string(request.query.lower_limit);
    const upper_limit = generalHelper.mysql_real_escape_string(request.query.upper_limit);

    if(parseInt(customer_id) === request.user.id) {
        Consultation.getMessages(consultation_id, lower_limit, upper_limit, (err, data) => {
            if(err)
                response.status(err.code).send(err.description);
            else
                response.status(data.code).send(data);
        });
    }
    else {
        return response.status(403).send({description: "You are not authorized to perform this action"});
    }
}

exports.addMessage = (request, response) => {
    if(!request.body || generalHelper.isEmptyObject(request.body)) {
        response.status(400).send({
            description: "Invalid request format!"
        });
        return;
    }

    // if(!request.body.customer_id || request.body.customer_id === "" || !request.body.consultation_id || request.body.consultation_id === "" || !request.body.category || request.body.category === "" || !request.body.sender_type || request.body.sender_type === "" || !request.body.message || !request.body.media) {
    //     return response.status(400).send({description: "The request was found to have an invalid format."});
    // }

    const customer_id = generalHelper.mysql_real_escape_string(request.body.customer_id);
    const consultation_id = generalHelper.mysql_real_escape_string(request.body.consultation_id);
    const category = generalHelper.mysql_real_escape_string(request.body.category);
    const sender_type = generalHelper.mysql_real_escape_string(request.body.sender_type);
    const message = generalHelper.mysql_real_escape_string(request.body.message);
    const media = generalHelper.mysql_real_escape_string(request.body.media);

    if(parseInt(customer_id) === request.user.id) {
        Consultation.addMessage(consultation_id, category, sender_type, message, media, (err, data) => {
            if(err)
                response.status(err.code).send(err.description);
            else
                response.status(data.code).send(data);
        });
    }
    else {
        return response.status(403).send({description: "You are not authorized to perform this action"});
    }
}

exports.reopen = (request, response) => {
    if(!request.body || generalHelper.isEmptyObject(request.body)) {
        response.status(400).send({
            description: "Invalid request format!"
        });
        return;
    }

    if(!request.body.customer_id || request.body.customer_id === "" || !request.body.consultation_id || request.body.consultation_id === "") {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    const customer_id = request.body.customer_id;
    const consultation_id = request.body.consultation_id;

    if(parseInt(customer_id) === request.user.id) {
        Consultation.reopen(consultation_id, (err, data) => {
            if(err)
                response.status(err.code).send(err.description);
            else
                response.status(data.code).send(data);
        });
    }
    else {
        return response.status(403).send({description: "You are not authorized to perform this action"});
    }
}

exports.read = (request, response) => {
    if(!request.body || generalHelper.isEmptyObject(request.body)) {
        response.status(400).send({
            description: "Invalid request format!"
        });
        return;
    }

    if(!request.body.customer_id || request.body.customer_id === "" || !request.body.consultation_id || request.body.consultation_id === "") {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    const customer_id = request.body.customer_id;
    const consultation_id = request.body.consultation_id;

    if(parseInt(customer_id) === request.user.id) {
        Consultation.read(consultation_id, (err, data) => {
            if(err)
                response.status(err.code).send(err.description);
            else
                response.status(data.code).send(data);
        });
    }
    else {
        return response.status(403).send({description: "You are not authorized to perform this action"});
    }
}

exports.verify = (request, response) => {
    if(!request.query || generalHelper.isEmptyObject(request.query)) {
        response.status(400).send({
            description: "Invalid request format!"
        });
        return;
    }

    if(!request.query.counsellor_id || request.query.counsellor_id === "" || !request.query.customer_id || request.query.customer_id === "" || !request.query.room_id || request.query.room_id === "" || !request.query.room_type || request.query.room_type === "") {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    const counsellor_id = request.query.counsellor_id;
    const customer_id = request.query.customer_id;
    const room_id = request.query.room_id;
    const room_type = request.query.room_type;

    Consultation.verify(counsellor_id, customer_id, room_id, room_type, (err, data) => {
        if(err)
            response.status(err.code).send(err.description);
        else
            response.status(data.code).send(data);
    });
}

exports.addBusyCallDeclined = (request, response) => {
    if(!request.body || generalHelper.isEmptyObject(request.body)) {
        response.status(400).send({
            description: "Invalid request format!"
        });
        return;
    }

    // if(!request.body.customer_id || request.body.customer_id === "" || !request.body.consultation_id || request.body.consultation_id === "" || !request.body.category || request.body.category === "" || !request.body.sender_type || request.body.sender_type === "" || !request.body.message || !request.body.media) {
    //     return response.status(400).send({description: "The request was found to have an invalid format."});
    // }

    const counsellor_id = generalHelper.mysql_real_escape_string(request.body.counsellor_id);
    const customer_id = generalHelper.mysql_real_escape_string(request.body.customer_id);
    const category = generalHelper.mysql_real_escape_string(request.body.category);
    const sender_type = generalHelper.mysql_real_escape_string(request.body.sender_type);
    const message = generalHelper.mysql_real_escape_string(request.body.message);
    const media = generalHelper.mysql_real_escape_string(request.body.media);

    if(parseInt(customer_id) === request.user.id) {
        Consultation.addBusyCallDeclined(counsellor_id, customer_id, category, sender_type, message, media, (err, data) => {
            if(err)
                response.status(err.code).send(err.description);
            else
                response.status(data.code).send(data);
        });
    }
    else {
        return response.status(403).send({description: "You are not authorized to perform this action"});
    }
}
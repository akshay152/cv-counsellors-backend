const State = require("../models/State.model");
const generalHelper = require("../helpers/GeneralHelper");

exports.getByCountry = (request, response) => {
    if(!request.query || generalHelper.isEmptyObject(request.query)) {
        return response.status(400).send({description: "Invalid request format!"});
    }

    if(!request.query.countryCode || request.query.countryCode === "") {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    const countryCode = generalHelper.mysql_real_escape_string(request.query.countryCode);

    State.getByCountry(countryCode, (err, data) => {
        if(err)
            response.status(err.code).send(err.description);
        else
            response.status(data.code).send(data.description);
    });
}
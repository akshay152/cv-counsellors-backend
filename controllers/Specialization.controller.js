const Specialization = require("../models/Specialization.model");
const generalHelper = require("../helpers/GeneralHelper");

exports.getForCourseAndMode = (request, response) => {

    if(!request.query || generalHelper.isEmptyObject(request.query)) {
        response.status(400).send({
            description: "Invalid request format!"
        });
        return;
    }

    if(!request.query.mode_id || request.query.mode_id === "" || !request.query.course_id || request.query.course_id === "") {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    const mode_id = generalHelper.mysql_real_escape_string(request.query.mode_id);
    const course_id = generalHelper.mysql_real_escape_string(request.query.course_id);

    Specialization.getForCourseAndMode(mode_id, course_id, (err, data) => {
        if(err)
            response.status(err.code).send(err.description);
        else
            response.status(data.code).send(data.description);
    });
}
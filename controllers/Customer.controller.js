const Customer = require("../models/Customer.model");
const generalHelper = require("../helpers/GeneralHelper");

exports.refreshAccessTokens = (request, response) => {
    if(!request.body || generalHelper.isEmptyObject(request.body)) {
        return response.status(400).send({description: "Invalid request format!"});
    }

    if(!request.body.refreshAccessToken || request.body.refreshAccessToken === "") {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    const refreshAccessToken = generalHelper.mysql_real_escape_string(request.body.refreshAccessToken);

    Customer.refreshAccessTokens(refreshAccessToken, (err, data) => {
        if(err)
            response.status(err.code).send(err.description);
        else
            response.status(data.code).send(data);
        });
}

exports.create = (request, response) => {
    if(!request.body || generalHelper.isEmptyObject(request.body)) {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    if(!request.body.name || request.body.name === "" || !request.body.mobile || request.body.mobile === "" || !request.body.password || request.body.password === "" || !request.body.timezone || request.body.timezone === "" || !request.body.country_id || request.body.country_id === "") {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    const name = generalHelper.mysql_real_escape_string(request.body.name);
    const mobile = generalHelper.mysql_real_escape_string(request.body.mobile);
    const password = generalHelper.mysql_real_escape_string(request.body.password);
    const timezone = generalHelper.mysql_real_escape_string(request.body.timezone);
    const country = generalHelper.mysql_real_escape_string(request.body.country_id);

    Customer.create(name, mobile, password, timezone, country, (err, data) => {
        if(err)
            response.status(err.code).send(err.description);
        else
            response.status(data.code).send(data.description);
    });
}

exports.login = (request, response) => {
    if(!request.body || generalHelper.isEmptyObject(request.body)) {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    if(!request.body.mobile || request.body.mobile === "" || !request.body.password || request.body.password === "") {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    const mobile = generalHelper.mysql_real_escape_string(request.body.mobile);
    const password = generalHelper.mysql_real_escape_string(request.body.password);

    Customer.login(mobile, password, (err, data) => {
        if(err)
            response.status(err.code).send(err.description);
        else
            response.status(data.code).send(data);
    });
}

exports.details = (request, response) => {
    if(!request.params || generalHelper.isEmptyObject(request.params)) {
        response.status(400).send({
            description: "Invalid request format!"
        });
        return;
    }

    if(!request.params.user_id || request.params.user_id === "") {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    const user_id = generalHelper.mysql_real_escape_string(request.params.user_id);

    Customer.details(user_id, (err, data) => {
        if(err)
            response.status(err.code).send(err.description);
        else
            response.status(data.code).send(data.result);
    });
}

exports.update = (request, response) => {
    if(!request.body || generalHelper.isEmptyObject(request.body) || !request.params || generalHelper.isEmptyObject(request.params)) {
        response.status(400).send({
            description: "Invalid request format!"
        });
        return;
    }

    if(!request.params.user_id || request.params.user_id === "" || !request.body.name || request.body.name === "" || !request.body.country_id || request.body.country_id === "" || !request.body.secondary_mobile || !request.body.gender || !request.body.dob || !request.body.timezone || !request.body.house_number || !request.body.colony_locality || !request.body.city || !request.body.state || !request.body.pincode || !request.body.pincode) {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    const user_id = generalHelper.mysql_real_escape_string(request.params.user_id);

    const name = generalHelper.mysql_real_escape_string(request.body.name);
    const secondary_mobile = generalHelper.mysql_real_escape_string(request.body.secondary_mobile);
    const gender = generalHelper.mysql_real_escape_string(request.body.gender);
    const dob = generalHelper.mysql_real_escape_string(request.body.dob);
    const timezone = generalHelper.mysql_real_escape_string(request.body.timezone);
    const house_number = generalHelper.mysql_real_escape_string(request.body.house_number);
    const colony_locality = generalHelper.mysql_real_escape_string(request.body.colony_locality);
    const city = generalHelper.mysql_real_escape_string(request.body.city);
    const state = generalHelper.mysql_real_escape_string(request.body.state);
    const country_id = generalHelper.mysql_real_escape_string(request.body.country_id);
    const pincode = generalHelper.mysql_real_escape_string(request.body.pincode);
    const language = generalHelper.mysql_real_escape_string(request.body.language);

    if(parseInt(user_id) === request.user.id) {
        Customer.update(user_id, name, secondary_mobile, gender, dob, timezone, house_number, colony_locality, city, state, country_id, pincode, language, (err, data) => {
            if(err)
                response.status(err.code).send(err.description);
            else
                response.status(data.code).send(data);
        });
    }
    else {
        return response.status(403).send({description: "You are not authorized to perform this action"});
    }
}

exports.updateImage = (request, response) => {
    if(!request.body || generalHelper.isEmptyObject(request.body) || !request.params || generalHelper.isEmptyObject(request.params)) {
        response.status(400).send({
            description: "Invalid request format!"
        });
        return;
    }

    if(!request.params.user_id || request.params.user_id === "" || !request.body.profile_image || request.body.profile_image === "") {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    const user_id = generalHelper.mysql_real_escape_string(request.params.user_id);
    const profile_image = generalHelper.mysql_real_escape_string(request.body.profile_image);

    if(parseInt(user_id) === request.user.id) {
        Customer.updateImage(user_id, profile_image, (err, data) => {
            if(err)
                response.status(err.code).send(err.description);
            else
                response.status(data.code).send(data);
        });
    }
    else {
        return response.status(403).send({description: "You are not authorized to perform this action"});
    }
}

exports.removeImage = (request, response) => {
    if(!request.params || generalHelper.isEmptyObject(request.params)) {
        response.status(400).send({
            description: "Invalid request format!"
        });
        return;
    }

    if(!request.params.user_id || request.params.user_id === "") {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    const user_id = generalHelper.mysql_real_escape_string(request.params.user_id);

    if(parseInt(user_id) === request.user.id) {
        Customer.removeImage(user_id, (err, data) => {
            if(err)
                response.status(err.code).send(err.description);
            else
                response.status(data.code).send(data);
        });
    }
    else {
        return response.status(403).send({description: "You are not authorized to perform this action"});
    }
}

exports.updateEmail = (request, response) => {
    if(!request.body || generalHelper.isEmptyObject(request.body)) {
        response.status(400).send({
            description: "Invalid request format!"
        });
        return;
    }

    if(!request.body.user_id || request.body.user_id === "" || !request.body.email_address || request.body.email_address === "" || !request.body.name || request.body.name === "") {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    const user_id = generalHelper.mysql_real_escape_string(request.body.user_id);
    const name = generalHelper.mysql_real_escape_string(request.body.name);
    const email_address = generalHelper.mysql_real_escape_string(request.body.email_address);

    if(parseInt(user_id) === request.user.id) {
        Customer.updateEmail(user_id, name, email_address, (err, data) => {
            if(err)
                response.status(err.code).send(err.description);
            else
                response.status(data.code).send(data);
        });
    }
    else {
        return response.status(403).send({description: "You are not authorized to perform this action"});
    }
}

exports.verifyEmail = (request, response) => {
    if(!request.query || generalHelper.isEmptyObject(request.query)) {
        response.status(400).send({
            description: "Invalid request format!"
        });
        return;
    }

    if(!request.query.verification_token || request.query.verification_token === "" || !request.query.email || request.query.email === "") {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    const email = generalHelper.mysql_real_escape_string(request.query.email);
    const verification_token = generalHelper.mysql_real_escape_string(request.query.verification_token);

    Customer.verifyEmail(email, verification_token, (err, data) => {
        if(err)
            response.status(err.code).send(err.description);
        else
            response.status(data.code).send(data);
    });
}

exports.getFeedbacks = (request, response) => {
    if(!request.params || generalHelper.isEmptyObject(request.params)) {
        response.status(400).send({
            description: "Invalid request format!"
        });
        return;
    }

    if(!request.params.user_id || request.params.user_id === "") {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    const user_id = generalHelper.mysql_real_escape_string(request.params.user_id);

    if(parseInt(user_id) === request.user.id) {
        Customer.getFeedbacks(user_id, (err, data) => {
            if(err)
                response.status(err.code).send(err.description);
            else
                response.status(data.code).send(data);
        });
    }
    else {
        return response.status(403).send({description: "You are not authorized to perform this action"});
    }
}

exports.logout = (request, response) => {
    if(!request.body || generalHelper.isEmptyObject(request.body)) {
        response.status(400).send({
            description: "Invalid request format!"
        });
        return;
    }

    if(!request.body.refreshAccessToken || request.body.refreshAccessToken === "") {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    const refreshAccessToken = generalHelper.mysql_real_escape_string(request.body.refreshAccessToken);

    Customer.logout(refreshAccessToken, (err, data) => {
        if(err)
            response.status(err.code).send(err.description);
        else
            response.status(data.code).send(data);
    });
}
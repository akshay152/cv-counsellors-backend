const Counsellor = require("../models/Counsellor.model");
const generalHelper = require("../helpers/GeneralHelper");

exports.getDetails = (request, response) => {

    if(!request.params || generalHelper.isEmptyObject(request.params)) {
        response.status(400).send({
            description: "Invalid request format!"
        });
        return;
    }

    if(!request.params.counsellor_slug || request.params.counsellor_slug === "") {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    const counsellor_slug = generalHelper.mysql_real_escape_string(request.params.counsellor_slug);

    Counsellor.getDetails(counsellor_slug, (err, data) => {
        if(err)
            response.status(err.code).send(err.description);
        else
            response.status(data.code).send(data.description);
    });
}

exports.getDetailsByID = (request, response) => {

    if(!request.params || generalHelper.isEmptyObject(request.params)) {
        response.status(400).send({
            description: "Invalid request format!"
        });
        return;
    }

    if(!request.params.counsellor_id || request.params.counsellor_id === "") {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    const counsellor_id = generalHelper.mysql_real_escape_string(request.params.counsellor_id);

    Counsellor.getDetailsByID(counsellor_id, (err, data) => {
        if(err)
            response.status(err.code).send(err.description);
        else
            response.status(data.code).send(data.description);
    });
}

exports.createReview = (request, response) => {
    if(!request.body || generalHelper.isEmptyObject(request.body)) {
        response.status(400).send({
            description: "Invalid request format!"
        });
        return;
    }

    if(!request.body.counsellor_id || request.body.counsellor_id === "" || !request.body.customer_id || request.body.customer_id === "" || !request.body.rating || request.body.rating === "") {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    const counsellor_id = generalHelper.mysql_real_escape_string(request.body.counsellor_id);
    const customer_id = generalHelper.mysql_real_escape_string(request.body.customer_id);
    const rating = generalHelper.mysql_real_escape_string(request.body.rating);
    const recommendation = generalHelper.mysql_real_escape_string(request.body.recommendation);
    const title = generalHelper.mysql_real_escape_string(request.body.title);
    const review = generalHelper.mysql_real_escape_string(request.body.review);
    const improvement = generalHelper.mysql_real_escape_string(request.body.improvement);

    if(parseInt(customer_id) === request.user.id) {
        Counsellor.createReview(counsellor_id, customer_id, rating, recommendation, title, review, improvement, (err, data) => {
            if(err)
                response.status(err.code).send(err.description);
            else
                response.status(data.code).send(data);
        });
    }
    else {
        return response.status(403).send({description: "You are not authorized to perform this action"});
    }
}

exports.updateReview = (request, response) => {
    if(!request.body || generalHelper.isEmptyObject(request.body)) {
        response.status(400).send({
            description: "Invalid request format!"
        });
        return;
    }

    if(!request.body.customer_id || request.body.customer_id === "" || !request.body.rating || request.body.rating === "") {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    const customer_id = generalHelper.mysql_real_escape_string(request.body.customer_id);
    const feedbackID = generalHelper.mysql_real_escape_string(request.body.feedback_id);
    const rating = generalHelper.mysql_real_escape_string(request.body.rating);
    const title = generalHelper.mysql_real_escape_string(request.body.title);
    const review = generalHelper.mysql_real_escape_string(request.body.review);

    if(parseInt(customer_id) === request.user.id) {
        Counsellor.updateReview(feedbackID, rating, title, review, (err, data) => {
            if(err)
                response.status(err.code).send(err.description);
            else
                response.status(data.code).send(data);
        });
    }
    else {
        return response.status(403).send({description: "You are not authorized to perform this action"});
    }
}

exports.deleteReview = (request, response) => {
    if(!request.query || generalHelper.isEmptyObject(request.query)) {
        response.status(400).send({
            description: "Invalid request format!"
        });
        return;
    }

    if(!request.query.customer_id || request.query.customer_id === "" || !request.query.feedback_id || request.query.feedback_id === "") {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    const customer_id = generalHelper.mysql_real_escape_string(request.query.customer_id);
    const feedbackID = generalHelper.mysql_real_escape_string(request.query.feedback_id);

    if(parseInt(customer_id) === request.user.id) {
        Counsellor.deleteReview(feedbackID, (err, data) => {
            if(err)
                response.status(err.code).send(err.description);
            else
                response.status(data.code).send(data);
        });
    }
    else {
        return response.status(403).send({description: "You are not authorized to perform this action"});
    }
}

exports.getConsultations = (request, response) => {
    if(!request.params || generalHelper.isEmptyObject(request.params)) {
        response.status(400).send({
            description: "Invalid request format!"
        });
        return;
    }

    if(!request.params.counsellor_slug || request.params.counsellor_slug === "") {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    const counsellor_slug = generalHelper.mysql_real_escape_string(request.params.counsellor_slug);

    Counsellor.getConsultations(counsellor_slug, (err, data) => {
        if(err)
            response.status(err.code).send(err.description);
        else
            response.status(data.code).send(data.description);
    });
}

exports.getCourses = (request, response) => {
    if(!request.params || generalHelper.isEmptyObject(request.params)) {
        response.status(400).send({
            description: "Invalid request format!"
        });
        return;
    }

    if(!request.params.counsellor_id || request.params.counsellor_id === "") {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    const counsellor_id = generalHelper.mysql_real_escape_string(request.params.counsellor_id);

    Counsellor.getCourses(counsellor_id, (err, data) => {
        if(err)
            response.status(err.code).send(err.description);
        else
            response.status(data.code).send(data.description);
    });
}

exports.getCounsellorModesForACourse = (request, response) => {
    if(!request.params || generalHelper.isEmptyObject(request.params)) {
        response.status(400).send({
            description: "Invalid request format!"
        });
        return;
    }

    if(!request.params.counsellor_id || request.params.counsellor_id === "" || !request.params.course_short_name || request.params.course_short_name === "") {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    const counsellor_id = generalHelper.mysql_real_escape_string(request.params.counsellor_id);
    const course_short_name = generalHelper.mysql_real_escape_string(request.params.course_short_name);

    Counsellor.getCounsellorModesForACourse(counsellor_id, course_short_name, (err, data) => {
        if(err)
            response.status(err.code).send(err.description);
        else
            response.status(data.code).send(data.description);
    });
}

exports.getCounsellorSpecializationsForModeCourse = (request, response) => {
    if(!request.params || generalHelper.isEmptyObject(request.params)) {
        response.status(400).send({
            description: "Invalid request format!"
        });
        return;
    }

    if(!request.params.course_short_name || request.params.course_short_name === "" || !request.params.mode_id || request.params.mode_id === "") {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    const course_short_name = generalHelper.mysql_real_escape_string(request.params.course_short_name);
    const mode_id = generalHelper.mysql_real_escape_string(request.params.mode_id);

    Counsellor.getCounsellorSpecializationsForModeCourse(course_short_name, mode_id, (err, data) => {
        if(err)
            response.status(err.code).send(err.description);
        else
            response.status(data.code).send(data.description);
    });
}

exports.getFromFilter = (request, response) => {
    if(!request.body || generalHelper.isEmptyObject(request.body)) {
        response.status(400).send({
            description: "Invalid request format!"
        });
        return;
    }

    if(!request.body.course_short_name || !request.body.mode_id || !request.body.sort_method) {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    const course_short_name = generalHelper.mysql_real_escape_string(request.body.course_short_name);
    const mode_id = generalHelper.mysql_real_escape_string(request.body.mode_id);
    const genders = generalHelper.mysql_real_escape_string(request.body.genders);
    const sort_method = generalHelper.mysql_real_escape_string(request.body.sort_method);

    Counsellor.getFromFilter(course_short_name, mode_id, genders, sort_method, (err, data) => {
        if(err)
            response.status(err.code).send(err.description);
        else
            response.status(data.code).send(data.description);
    });
}
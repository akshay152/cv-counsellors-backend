const Country = require("../models/Country.model");

exports.getCountryCode = (request, response) => {

    Country.getCountryCode((err, data) => {
        if(err)
            response.status(err.code).send(err.description);
        else
            response.status(data.code).send(data.description);
    });
}
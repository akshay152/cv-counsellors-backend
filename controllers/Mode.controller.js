const Mode = require("../models/Mode.model");
const generalHelper = require("../helpers/GeneralHelper");

exports.getForCourse = (request, response) => {

    if(!request.params || generalHelper.isEmptyObject(request.params)) {
        response.status(400).send({
            description: "Invalid request format!"
        });
        return;
    }

    if(!request.params.course_short_name || request.params.course_short_name === "") {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    const course_short_name = generalHelper.mysql_real_escape_string(request.params.course_short_name);

    Mode.getForCourse(course_short_name, (err, data) => {
        if(err)
            response.status(err.code).send(err.description);
        else
            response.status(data.code).send(data.description);
    });
}

exports.getDescription = (request, response) => {
    if(!request.params || generalHelper.isEmptyObject(request.params)) {
        response.status(400).send({
            description: "Invalid request format!"
        });
        return;
    }

    if(!request.params.mode_id || request.params.mode_id === "") {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    const mode_id = generalHelper.mysql_real_escape_string(request.params.mode_id);

    Mode.getDescription(mode_id, (err, data) => {
        if(err)
            response.status(err.code).send(err.description);
        else
            response.status(data.code).send(data.description);
    });
}
const express = require("express");
const fileUpload = require('express-fileupload');
const http = require("http");
const cors = require("cors");
const s3 = require("./config/aws.config");
const Razorpay = require("razorpay");
const generalHelper = require("./helpers/GeneralHelper");

const conn = require('./config/db.config');
const { cryptPassword } = require("./config/hasher.config");

const app = express();

app.use(express.json({limit: "200mb"}));
app.use(express.urlencoded({extended: true}));
app.use(fileUpload());

const httpServer = http.createServer(app);

const io = require("socket.io")(httpServer, {
    cors: {
        origin: "*",
        methods: ["GET", "POST", "PUT", "DELETE"]
    }
});

app.use(cors());

const PORT = process.env.PORT || 9000;

app.get('/', (req, res) => {
    res.send('Running');
});

app.get('/collegevidya/course-spec', (request, response) => {
    if(!request.query || generalHelper.isEmptyObject(request.query)) {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    if(!request.query.course || request.query.course === "")  {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    const course = generalHelper.mysql_real_escape_string(request.query.course);
    var specialization = "";
    if(request.query.specialization) {
        specialization = generalHelper.mysql_real_escape_string(request.query.specialization);
    }

    conn.query(`SELECT id FROM courses WHERE short_name = '${course}'`, (error, res) => {
        if(error) {
            return response.status(500).send({description: error});
        }
        var course_id = res[0].id;

        if(specialization !== "") {
            conn.query(`SELECT id FROM specializations WHERE name LIKE '${specialization}%'`, (error, resp) => {
                if(error) {
                    return response.status(500).send({description: error});
                }

                var specialization_id = "";

                if(resp.length > 0) {
                    specialization_id = resp[0].id;
                    return response.status(200).send({course_id: course_id, specialization_id: specialization_id});
                }
                else {
                    conn.query(`SELECT id FROM specializations WHERE course_id = ${course_id}`, (error, reso) => {
                        if(error) {
                            return response.status(500).send({description: error});
                        }
                        specialization_id = reso[0].id;
                        return response.status(200).send({course_id: course_id, specialization_id: specialization_id});
                    })
                }
            })
        }
        else {
            conn.query(`SELECT id FROM specializations WHERE course_id = ${course_id}`, (error, reso) => {
                if(error) {
                    return response.status(500).send({description: error});
                }
                var specialization_id = reso[0].id;

                return response.status(200).send({course_id: course_id, specialization_id: specialization_id});
            })
        }
    })
})

app.get('/collegevidya/redirect/login', (request, response) => {
    if(!request.query || generalHelper.isEmptyObject(request.query)) {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    if(!request.query.cv_id || request.query.cv_id === "")  {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    const cv_id = generalHelper.mysql_real_escape_string(request.query.cv_id);

    conn.query(`SELECT mobile, password FROM customers WHERE cv_id = '${cv_id}'`, (error, mainRes) => {
        if(error) {
            return response.status(500).send({description: error});
        }
        if(mainRes.length === 0) {
            return response.status(401).send({description: "No such account"});
        }
        else {
            return response.status(200).send({mobile: mainRes[0].mobile});
        }
    })
})

app.get("/collegevidya/redirect/active-consultations", (request, response) => {
    if(!request.query || generalHelper.isEmptyObject(request.query)) {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    if(!request.query.cv_id || request.query.cv_id === "" || !request.query.specialization_id || request.query.specialization_id === "")  {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    const cv_id = generalHelper.mysql_real_escape_string(request.query.cv_id);
    const specialization_id = generalHelper.mysql_real_escape_string(request.query.specialization_id);

    conn.query(`SELECT consultations.id as active_consultations, consultations.specialization_ids, customers.name FROM consultations LEFT JOIN customers ON customers.id = consultations.customer_id WHERE customers.cv_id = '${cv_id}' AND consultations.finished <> 1 AND ${specialization_id} IN (consultations.specialization_ids)`, (error, res) => {
        if(error) {
            return response.status(500).send({description: error});
        }
        return response.status(200).send(res);
    })
});

app.post('/collegevidya/customer/create', (request, response) => {
    if(!request.body || generalHelper.isEmptyObject(request.body)) {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    if(!request.body.gender || request.body.gender === "" || !request.body.name || request.body.name === "" || !request.body.mobile || request.body.mobile === "" || !request.body.email || request.body.email === "" || !request.body.password || request.body.password === "" || !request.body.timezone || request.body.timezone === "" || !request.body.country || request.body.country === "" || !request.body.collegevidya_id || request.body.collegevidya_id === "") {
        return response.status(400).send({description: "The request was found to have an invalid format."});
    }

    const gender = generalHelper.mysql_real_escape_string(request.body.gender);
    const name = generalHelper.mysql_real_escape_string(request.body.name);
    const mobile = generalHelper.mysql_real_escape_string(request.body.mobile);
    const email = generalHelper.mysql_real_escape_string(request.body.email);
    const password = generalHelper.mysql_real_escape_string(request.body.password);
    const timezone = generalHelper.mysql_real_escape_string(request.body.timezone);
    const country = generalHelper.mysql_real_escape_string(request.body.country);
    const collegevidya_id = generalHelper.mysql_real_escape_string(request.body.collegevidya_id);

    conn.query(`SELECT id FROM customers WHERE mobile = '${mobile}' OR email = '${email}'`, (error, mainRes) => {
        if(error) {
            return response.status(500).send({description: error});
        }
        if(mainRes.length === 0) {
            conn.query(`SELECT id FROM countries WHERE name = '${country}'`, (error, res) => {
                if(error) {
                    return response.status(500).send({description: error});
                }
                const country_id = res[0].id;

                cryptPassword(password, (error, enc_pass) => {
                    if(error) {
                        return response.status(500).send({description: error});
                    }
                    conn.query(`INSERT INTO customers (gender, name, mobile, email, password, timezone, country_id, cv_id) VALUES ('${gender}', '${name}', '${mobile}', '${email}', '${enc_pass}', '${timezone}', ${country_id}, '${collegevidya_id}')`, (error, res) => {
                        if(error) {
                            return response.status(500).send({description: error});
                        }
                        return response.status(201).send();
                    });
                });
            });
        }
        else {
            conn.query(`SELECT id FROM countries WHERE name = '${country}'`, (error, res) => {
                if(error) {
                    return response.status(500).send({description: error});
                }
                const country_id = res[0].id;

                cryptPassword(password, (error, enc_pass) => {
                    if(error) {
                        return response.status(500).send({description: error});
                    }
                    conn.query(`UPDATE customers SET gender = '${gender}', name = '${name}', mobile = '${mobile}', email = '${email}', password = '${enc_pass}', timezone = '${timezone}', country_id = ${country_id}, cv_id = '${collegevidya_id}' WHERE id = ${mainRes[0].id}`, (error, res) => {
                        if(error) {
                            return response.status(500).send({description: error});
                        }
                        return response.status(204).send();
                    });
                });
            });
        }
    })
});

app.get('/s3URL', generalHelper.authenticateToken, async (req, res) => {

    let objectType = s3.folders.PROFILE_IMAGES;
    let objectExtension = "";

    if(req.query.object && req.query.object === "image") {
        objectType = s3.folders.PROFILE_IMAGES;
    }
    else if (req.query.object && req.query.object === "voice") {
        objectType = s3.folders.VOICE_CALLS;
    }
    else if (req.query.object && req.query.object === "video") {
        objectType = s3.folders.VIDEO_CALLS;
    }
    else if (req.query.object && req.query.object === "chat-doc") {
        objectType = s3.folders.CHAT_DOCUMENTS;
        if(req.query.type) {
            objectExtension = req.query.type;
        }
    }

    const url = await s3.generateUploadURL(objectType, objectExtension);
    res.send({url});
});

//create-consultation-razorpay
app.post("/consultation/create/instant", generalHelper.authenticateToken, async (request, response) => {
    try {
        const instance = new Razorpay({
            key_id: process.env.RAZORPAY_KEY_ID,
            key_secret: process.env.RAZORPAY_SECRET,
        });

        const options = {
            amount: request.body.amount,
            currency: request.body.currency,
            receipt: request.body.receipt,
        };

        const order = await instance.orders.create(options);

        if(order) {
            response.status(200).send(order);
        }
        else {
            response.status(500).send({"code": "ORDER ERROR", "message": "Something went wrong"});
        }
    }
    catch(error) {
        response.status(500).send({"code": "LIBRARY ERROR", "message": error});
    }
})

//Add Customer routes
require("./routes/Customer.route")(app);

//Add Counsellor routes
require("./routes/Counsellor.route")(app);

//Add Mode routes
require("./routes/Mode.route")(app);

//Add Program Category routes
require("./routes/ProgramCategory.route")(app);

//Add Course routes
require("./routes/Course.route")(app);

//Add Specilization routes
require("./routes/Specilization.route")(app);

//Add State routes
require("./routes/State.route")(app);

//Add Country routes
require("./routes/Country.route")(app);

//Add Review routes
require("./routes/Review.route")(app);

//Add consultation routes
require("./routes/Consultation.route")(app);

//Add misc routes
require("./routes/Misc.route")(app);

io.on("connection", socket => {
    console.log(`New user joined: ${socket.id}`);
});

httpServer.listen(PORT, () => console.log(`Server is running on port ${PORT}`));
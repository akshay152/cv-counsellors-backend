require('dotenv').config();

const aws = require("aws-sdk");
const crypto = require("crypto");
const util = require("util");

const region = "ap-south-1";
const bucketName = "cv-counsellors";
const accessKeyId = process.env.AWS_ACCESS_KEY;
const secretAccessKey = process.env.AWS_SECRET_ACCESS_KEY;

const randomBytes = util.promisify(crypto.randomBytes);

const s3 = new aws.S3({
    region, accessKeyId, secretAccessKey, signatureVersion: 'v4'
});

exports.folders = {
    PROFILE_IMAGES: "profile-images",
    VOICE_CALLS: "voice-calls",
    VIDEO_CALLS: "video-calls",
    CHAT_DOCUMENTS: "chat-documents"
}

exports.generateUploadURL = async function(objectType, objectExtension) {
    const rawBytes = await randomBytes(16);
    const imageName = rawBytes.toString('hex');

    const params = ({
        Bucket: bucketName,
        Key: objectExtension !== "" ? objectType+"/"+imageName+"."+objectExtension : objectType+"/"+imageName,
        Expires: 60
    });

    const uploadURL = await s3.getSignedUrlPromise('putObject', params);
    return uploadURL;
}
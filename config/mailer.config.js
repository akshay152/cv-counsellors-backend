const axios = require("axios");

const templates = [
    {
        id: 325,
        name: "WELCOME-MAIL"
    },
    {
        id: 326,
        name: "NEW-BOOKING-MAIL"
    },
    {
        id: 339,
        name: "NEW-BOOKING-MAIL-COUNSELLOR"
    },
    {
        id: 337,
        name: "THANK-YOU-FOR-FEEDBACK"
    },
    {
        id: 332,
        name: "CUSTOMER-COUNSELLING-REMINDER"
    },
    {
        id: 340,
        name: "COUNSELLOR-COUNSELLING-REMINDER"
    }
]

exports.shootEmail = async function(sender, to, subject, htmlContent) {
    var data = JSON.stringify({
        sender: sender,
        to: to,
        subject: subject,
        htmlContent: htmlContent
    });

    var config = {
        method: 'post',
        url: process.env.SENDINBLUE_URL,
        headers: { 
            'Accept': 'application/json', 
            'api-key': process.env.SENDINBLUE_API_KEY, 
            'Content-Type': 'application/json'
        },
        data : data
    };

    const mailStatus = await axios(config);
    return mailStatus;
};

exports.sendTemplateMail = async function(templateName, to, params) {
    var data = JSON.stringify({
         to: to,
         templateId: templates.filter(template => template.name === templateName)[0].id,
         params: params
    });

    var config = {
        method: 'post',
        url: process.env.SENDINBLUE_URL,
        headers: { 
            'Accept': 'application/json', 
            'api-key': process.env.SENDINBLUE_API_KEY, 
            'Content-Type': 'application/json'
        },
        data : data
    };

    const mailStatus = await axios(config);
    return mailStatus;
}
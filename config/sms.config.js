const axios = require("axios");

const templates = [
    {
        id: "1307165364559128568",
        name: "WELCOME-SMS",
    },
    {
        id: "1307165364371943945",
        name: "NEW-BOOKING-SMS"
    },
    {
        id: "1307165364538794434",
        name: "THANK-YOU-FOR-FEEDBACK-SMS"
    },
    {
        id: "1307165372045953777",
        name: "CUSTOMER-COUNSELLING-REMINDER-SMS"
    }
];

exports.sendTemplateSMS = async function (templateName, receiver, encodedText) {
    var config = {
        method: 'get',
        url: process.env.VENETS_MEDIA_URL+`?user=${process.env.VENETS_MEDIA_USER}&authkey=${process.env.VENETS_MEDIA_AUTHKEY}&sender=${process.env.VENETS_MEDIA_SENDER}&mobile=${receiver}&text=${encodedText}&rpt=1&summary=1&output=json&templateid=${templates.filter(template => template.name === templateName)[0].id}`,
        headers: { }
    }

    const smsStatus = await axios(config);
    return smsStatus;
}
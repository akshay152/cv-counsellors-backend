const conn = require("../config/db.config");

const Specialization = function(specialization) {
    this.id = specialization.id;
}

Specialization.getForCourseAndMode = (mode_id, course_id, result) => {
    conn.query(`SELECT specializations.id, specializations.name from specializations LEFT JOIN courses on specializations.course_id = courses.id LEFT JOIN modes on courses.mode_id = modes.id WHERE courses.id = ${course_id} AND modes.id = ${mode_id} AND specializations.status = 1`, (error, res) => {
        if(error) {
            return result({code: 500, description: error}, null);
        }
        return result(null, {code: 200, description: res});
    });
}

module.exports = Specialization;
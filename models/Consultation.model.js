const conn = require("../config/db.config");
const shortid = require('shortid');
const { sendTemplateMail } = require("../config/mailer.config");
const moment = require("moment");
const axios = require("axios");
const { sendTemplateSMS } = require("../config/sms.config");

const Consultation = function (consultation) {
    this.id = consultation.id;
}

Consultation.create = (counsellor_id, customer_id, specialization_id, start_time, transaction_id, result) => {
    conn.query(`SELECT name, email FROM counsellors WHERE id = ${counsellor_id}`, (error, res) => {
        if (error) {
            return result({ code: 500, description: error }, null);
        }
        const counsellor_name = res[0].name;
        const counsellor_email = res[0].email;
        conn.query(`SELECT email, name, mobile, cv_id FROM customers WHERE id = ${customer_id}`, (error, res) => {
            if (error) {
                return result({ code: 500, description: error }, null);
            }
            const customer_name = res[0].name;
            const customer_email = res[0].email;
            const customer_number = res[0].mobile;
            const customer_cv_id = res[0].cv_id;
            conn.query(`SELECT id FROM consultations WHERE counsellor_id = ${counsellor_id} AND customer_id = ${customer_id} AND finished = 0`, (error, res) => {
                if (error) {
                    return result({ code: 500, description: error }, null);
                }
                if (res.length > 0) {
                    return result({ code: 422, description: "You already have an active consulation with " + counsellor_name }, null);
                }
                conn.query(`SELECT id FROM consultations WHERE start_time = '${start_time}' AND counsellor_id = '${counsellor_id}'`, (error, res) => {
                    if (error) {
                        return result({ code: 500, description: error }, null);
                    }
                    if (res.length > 0) {
                        return result({ code: 500, description: "This slot is already booked" }, null);
                    }
                    conn.query(`SELECT id FROM consultations WHERE counsellor_id = ${counsellor_id} AND customer_id = ${customer_id}`, (error, res) => {
                        if (error) {
                            return result({ code: 500, description: error }, null);
                        }

                        if (res.length > 0) {
                            const consultation_id = res[0].id;

                            conn.query(`UPDATE consultations SET specialization_ids = CONCAT(specialization_ids, ",", ${specialization_id}), start_time = '${start_time}', finished = 0 WHERE id = ${consultation_id}`, (error, res) => {
                                if (error) {
                                    return result({ code: 500, description: error }, null);
                                }
                                conn.query(`INSERT INTO consultation_messages (consultation_id, category, sender_type, message, unread) VALUES (${consultation_id}, 'chat-prompt', 'SYSTEM', "A new consulation has started", 0)`, (error, res) => {
                                    if (error) {
                                        return result({ code: 500, description: error }, null);
                                    }

                                    const to = [
                                        {
                                            name: customer_name,
                                            email: customer_email
                                        }
                                    ];

                                    const params = {
                                        id: customer_number,
                                        password: customer_cv_id,
                                        datetime: moment(start_time).format('LLL'),
                                        link: 'https://cvcounselor.com/user/consultations?consultation_id=' + consultation_id
                                    }

                                    axios.get(`https://api.millionverifier.com/api/v3/?api=41Qb2yB60xBox8hMqfZQjGy1h&email=${customer_email}&timeout=10`)
                                        .then((response) => {
                                            if (response.data.result === "ok") {
                                                sendTemplateMail("NEW-BOOKING-MAIL", to, params);
                                            }
                                        });

                                    sendTemplateSMS('NEW-BOOKING-SMS', customer_number, encodeURI(`Cheers! Your slot has been booked at the selected time on ${params.datetime}. Join Now: https://cvcounselor.com College Vidya- Your right university guide!`));

                                    const counsellorTo = [
                                        {
                                            name: counsellor_name,
                                            email: counsellor_email
                                        }
                                    ];

                                    const counsellorParams = {
                                        counsellor_name: counsellor_name,
                                        date: moment(start_time).format('LL'),
                                        time: moment(start_time).format('LT'),
                                        link: 'https://partner.cvcounselor.com/dashboard/consultations?consultation_id=' + consultation_id
                                    }

                                    axios.get(`https://api.millionverifier.com/api/v3/?api=41Qb2yB60xBox8hMqfZQjGy1h&email=${counsellor_email}&timeout=10`)
                                        .then((response) => {
                                            if (response.data.result === "ok") {
                                                sendTemplateMail("NEW-BOOKING-MAIL-COUNSELLOR", counsellorTo, counsellorParams);
                                            }
                                        });
                                    return result(null, { code: 200, description: consultation_id });
                                });
                            });
                        }
                        else {
                            conn.query(`INSERT INTO consultations (counsellor_id, customer_id, specialization_ids, start_time, video_room_id, audio_room_id, transaction_id) VALUES (${counsellor_id}, ${customer_id}, ${specialization_id}, '${start_time}', '${shortid.generate()}', '${shortid.generate()}', '${transaction_id}')`, (error, resp) => {
                                if (error) {
                                    return result({ code: 500, description: error }, null);
                                }
                                conn.query(`INSERT INTO consultation_messages (consultation_id, category, sender_type, message, unread) VALUES (${resp.insertId}, 'chat-prompt', 'SYSTEM', "Consultation has now started between ${counsellor_name} and ${customer_name}", 0)`, (error, res) => {
                                    if (error) {
                                        return result({ code: 500, description: error }, null);
                                    }

                                    const to = [
                                        {
                                            name: customer_name,
                                            email: customer_email
                                        }
                                    ];

                                    const params = {
                                        id: customer_number,
                                        password: customer_cv_id,
                                        datetime: moment(start_time).format('LLL'),
                                        link: 'https://cvcounselor.com/user/consultations?consultation_id=' + resp.insertId
                                    }

                                    axios.get(`https://api.millionverifier.com/api/v3/?api=41Qb2yB60xBox8hMqfZQjGy1h&email=${customer_email}&timeout=10`)
                                        .then((response) => {
                                            if (response.data.result === "ok") {
                                                sendTemplateMail("NEW-BOOKING-MAIL", to, params);
                                            }
                                        });

                                    sendTemplateSMS('NEW-BOOKING-SMS', customer_number, encodeURI(`Cheers! Your slot has been booked at the selected time on ${params.datetime}. Join Now: https://cvcounselor.com College Vidya- Your right university guide!`))

                                    const counsellorTo = [
                                        {
                                            name: counsellor_name,
                                            email: counsellor_email
                                        }
                                    ];

                                    const counsellorParams = {
                                        counsellor_name: counsellor_name,
                                        date: moment(start_time).format('LL'),
                                        time: moment(start_time).format('LT'),
                                        link: 'https://partner.cvcounselor.com/dashboard/consultations?consultation_id=' + resp.insertId
                                    }

                                    axios.get(`https://api.millionverifier.com/api/v3/?api=41Qb2yB60xBox8hMqfZQjGy1h&email=${counsellor_email}&timeout=10`)
                                        .then((response) => {
                                            if (response.data.result === "ok") {
                                                sendTemplateMail("NEW-BOOKING-MAIL-COUNSELLOR", counsellorTo, counsellorParams);
                                            }
                                        });

                                    return result(null, { code: 200, description: resp.insertId });
                                });
                            });
                        }
                    });
                });
            });
        });
    });
}

Consultation.reopen = (consultation_id, result) => {
    conn.query(`UPDATE consultations SET finished = 0 WHERE id = ${consultation_id}`, (error, res) => {
        if (error) {
            return result({ code: 500, description: error }, null);
        }
        return result(null, { code: 200, description: res });
    });
}

Consultation.getForACustomer = (customer_id, result) => {
    conn.query(`SELECT consultations.counsellor_id as counsellor_id, consultations.id as id, consultations.video_room_id as video_room_id, consultations.start_time as start_time, consultations.audio_room_id as audio_room_id, counsellors.profile_image as image, counsellors.name as name, counsellors.slug as slug, (SELECT CONCAT('[', GROUP_CONCAT(JSON_OBJECT('lastMessage', consultation_messages.message, 'lastMessageTime', consultation_messages.created_at, 'unread', consultation_messages.unread, 'sender_type', consultation_messages.sender_type, 'category', consultation_messages.category)),']') FROM consultation_messages WHERE consultation_messages.consultation_id = consultations.id GROUP BY consultation_messages.id ORDER BY consultation_messages.created_at DESC LIMIT 1) as lastMessageDetails, consultations.finished as finished FROM consultations LEFT JOIN counsellors ON counsellors.id = consultations.counsellor_id LEFT JOIN consultation_messages ON consultation_messages.consultation_id = consultations.id WHERE consultations.customer_id = ${customer_id} GROUP BY consultations.id`, (error, res) => {
        if (error) {
            return result({ code: 500, description: error }, null);
        }
        return result(null, { code: 200, description: res });
    });
}

Consultation.getMessages = (consultation_id, lower_limit, upper_limit, result) => {
    conn.query(`SELECT consultation_messages.id as id, consultation_messages.category as category, consultation_messages.sender_type as sender_type, consultation_messages.message as msg, consultation_messages.created_at as time, consultation_messages.media as media FROM consultation_messages WHERE consultation_messages.consultation_id = ${consultation_id} ORDER BY consultation_messages.created_at DESC LIMIT ${lower_limit}, ${upper_limit}`, (error, res) => {
        if (error) {
            return result({ code: 500, description: error }, null);
        }
        return result(null, { code: 200, description: res });
    });
}

Consultation.addMessage = (consultation_id, category, sender_type, message, media, result) => {
    conn.query(`INSERT INTO consultation_messages (consultation_id, category, sender_type, message, media) VALUES (${consultation_id}, '${category}', '${sender_type}', '${message}', '${media}')`, (error, res) => {
        if (error) {
            return result({ code: 500, description: error }, null);
        }
        return result(null, { code: 200, description: res });
    });
}

Consultation.read = (consultation_id, result) => {
    conn.query(`UPDATE consultation_messages SET unread = 0 WHERE sender_type = 'COUNSELLOR' AND consultation_id = ${consultation_id}`, (error, res) => {
        if (error) {
            return result({ code: 500, description: error }, null);
        }
        return result(null, { code: 200, description: res });
    });
}

Consultation.verify = (counsellor_id, customer_id, room_id, room_type, result) => {
    conn.query(`SELECT consultations.id as id FROM consultations WHERE consultations.counsellor_id = ${counsellor_id} AND consultations.customer_id = ${customer_id} AND consultations.${room_type} = '${room_id}' AND consultations.finished = 0`, (error, res) => {
        if (error) {
            return result({ code: 500, description: error }, null);
        }
        if (res.length === 0) {
            return result({ code: 422, description: "Consulation invalid or marked as finished" }, null);
        }
        return result(null, { code: 200, description: res });
    });
}

Consultation.addBusyCallDeclined = (counsellor_id, customer_id, category, sender_type, message, media, result) => {
    conn.query(`SELECT id FROM consultations WHERE counsellor_id = ${counsellor_id} AND customer_id = ${customer_id}`, (error, res) => {
        if (error) {
            return result({ code: 500, description: error }, null);
        }
        if (res.length === 0) {
            return result({ code: 422, description: "No such consultation" }, null);
        }
        conn.query(`INSERT INTO consultation_messages (consultation_id, category, sender_type, message, media, unread) VALUES (${res[0].id}, '${category}', '${sender_type}', '${message}', '${media}', 0)`, (error, res) => {
            if (error) {
                return result({ code: 500, description: error }, null);
            }
            return result(null, { code: 200, description: res });
        });
    })
}

module.exports = Consultation;
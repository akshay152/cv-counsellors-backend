const conn = require("../config/db.config");

const Review = function(review) {
    this.id = review.id;
}

Review.getForCounsellor = (counsellor_slug, result) => {
    conn.query(`SELECT counsellor_reviews.id as id, customers.name as name, DATE_FORMAT(counsellor_reviews.created_at, '%Y%m%d%h%i%s%p') AS timestamp, counsellor_reviews.rating as rating, counsellor_reviews.title as title, counsellor_reviews.review as review, counsellor_reviews.reply as reply, counsellor_reviews.is_verified as is_verified FROM counsellor_reviews LEFT JOIN customers ON customers.id = counsellor_reviews.customer_id LEFT JOIN counsellors ON counsellors.id = counsellor_reviews.counsellor_id WHERE counsellors.slug = '${counsellor_slug}' ORDER BY counsellor_reviews.created_at DESC`, (error, res) => {
        if(error) {
            return result({code: 500, description: error}, null);
        }
        return result(null, {code: 200, description: res});
    });
}

module.exports = Review;
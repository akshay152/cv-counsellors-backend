const jwt = require("jsonwebtoken");
const conn = require("../config/db.config");
const generalHelper = require("../helpers/GeneralHelper");
const passwordHasher = require("../config/hasher.config");
const crypto = require("crypto");
const fs = require("fs");
const Handlebars = require("handlebars");
const { shootEmail } = require("../config/mailer.config");
const path = require("path");

const Customer = function(customer) {
    this.id = customer.id;
}

let refreshAccessTokens = [];

Customer.refreshAccessTokens = (refreshToken, result) => {
    if(!refreshToken) {
        return result({code: 401, description: "No Refresh Access Token"});
    }
    if(!refreshAccessTokens.includes(refreshToken)) {
        return result({code: 401, description: "Invalid Refresh Access Token"});
    }
    jwt.verify(refreshToken, process.env.REFRESH_ACCESS_TOKEN_SECRET, (err, user) => {
        err && console.log(err);

        refreshAccessTokens = refreshAccessTokens.filter((token) => token !== refreshToken);

        const newAccessToken = generalHelper.generateAccessToken(user);
        const newRefreshAccessToken = generalHelper.generateRefreshAccessToken(user);

        refreshAccessTokens.push(newRefreshAccessToken);

        result(null, {code: 200, "accessToken": newAccessToken, "refreshAccessToken": newRefreshAccessToken});
    });
}

Customer.create = (name, mobile, password, timezone, country_id, result) => {
    passwordHasher.cryptPassword(password, (error, enc_pass) => {
        if(error) {
            return result({code: 500, description: error}, null);
        }
        conn.query(`SELECT id FROM customers WHERE mobile = '${mobile}'`, (err, res) => {
            if(err) {
                return result({code: 500, description: error}, null);
            }
            if(res.length > 0) {
                return result({code: 422, description: "An account with this mobile number already exists"}, null);
            }
            conn.query(`INSERT INTO customers(name, mobile, password, timezone, country_id) VALUES ('${name}', '${mobile}', '${enc_pass}', '${timezone}', '${country_id}')`, (err, res) => {
                if(err) {
                    return result({code: 500, description: err}, null);
                }
                return result(null, {code: 201});
            });
        });
    });
}

Customer.login = (mobile, password, result) => {
    conn.query(`SELECT * FROM customers WHERE mobile = '${mobile}'`, (err, res) => {
        if(err) {
            return result({code: 500, description: err}, null);
        }
        if(generalHelper.isEmptyObject(res)) {
            return result({code: 403, description: "Invalid credentials"});
        }
        passwordHasher.comparePassword(password, res[0].password, (err, isPasswordMatch) => {
            if(err) {
                return result({code: 403, description: "Invalid credentials"});
            }
            if(!isPasswordMatch) {
                return result({code: 403, description: "Invalid credentials"});
            }
            if(res[0].status === 0) {
                return result({code: 403, description: "Your account has been suspended"});
            }
            
            const accessToken = generalHelper.generateAccessToken(res[0]);
            const refreshAccessToken = generalHelper.generateRefreshAccessToken(res[0]);
            
            refreshAccessTokens.push(refreshAccessToken);
            const resp = {
                "code": 200,
                "user": res[0],
                "accessToken": accessToken,
                "refreshAccessToken": refreshAccessToken
            }
            
            return result(null, resp);
        });
    });
}

Customer.details = (user_id, result) => {
    conn.query(`SELECT * FROM customers WHERE id = ${user_id}`, (error, res) => {
        if(error) {
            return result({code: 500, description: error}, null);
        }
        else {
            result(null, {code: 200, result: res});
        }
    });
}

Customer.update = (user_id, name, secondary_mobile, gender, dob, timezone, house_number, colony_locality, city, state, country_id, pincode, language, result) => {
    conn.query(`UPDATE customers SET name = '${name}', secondary_mobile = '${secondary_mobile}', gender = '${gender}',  dob = '${dob}', timezone = '${timezone}', house_number = '${house_number}', colony_locality = '${colony_locality}', city = '${city}', state = '${state}',  country_id = ${country_id},  pincode = '${pincode}', language = '${language}' WHERE id = ${user_id}`, (error, res) => {
        if(error) {
            return result({code: 500, description: error}, null);
        }
        else {
            result(null, {code: 200, result: res});
        }
    });
}

Customer.updateEmail = (user_id, name, email_address, result) => {
    conn.query(`SELECT id FROM customers WHERE email = '${email_address}'`, (error, res) => {
        if(error) {
            return result({code: 500, description: error}, null);
        }
        if(!generalHelper.isEmptyObject(res)) {
            return result({code: 422, description: "An account with this email already exists"}, null);
        }
        else {
            var confirmation_token = crypto.randomBytes(32).toString('hex');
            conn.query(`UPDATE customers SET unverified_email = '${email_address}', email_verification_token = '${confirmation_token}' WHERE id = '${user_id}'`, (error) => {
                if(error) {
                    return result({code: 500, description: error}, null);
                }
                else {
                    var confirmation_link = `${process.env.BASE_URL}user/verify?email=${email_address}&verification_token=${confirmation_token}`;

                    var source = fs.readFileSync(path.resolve(__dirname, "../templates/account_confirmation.html"), 'utf-8');
                    var template = Handlebars.compile(source);
                    var data = {"confirmation_link": confirmation_link}

                    var htmlContent = template(data);

                    const sender = {
                        name: "CV Counsellors",
                        email: "no-reply@cvcounsellors.com"
                    };

                    const to = [
                        {
                            name: name,
                            email: email_address
                        }
                    ];

                    const subject = "Verify your email address";

                    shootEmail(sender, to, subject, htmlContent).then(() => {
                        return result({code: 200, description: confirmation_token});
                    })
                    .catch((error) => {
                        return result({code: 500, description: error});
                    });
                }
            });
        }
    })
}

Customer.verifyEmail = (email_address, verification_token, result) => {
    conn.query(`SELECT id FROM customers WHERE email_verification_token = '${verification_token}'`, (error, res) => {
        if(error || generalHelper.isEmptyObject(res)) {
            return result({code: 500, description: error}, null);
        }
        else {
            conn.query(`UPDATE customers SET email = '${email_address}', email_verification_token = '', unverified_email = '' WHERE email_verification_token = '${verification_token}'`, (error, res) => {
                if(error) {
                    return result({code: 500, description: error}, null);
                }
                else {
                    result(null, {code: 200, result: res});
                }
            })
        }
    })
}

Customer.updateImage = (user_id, profile_image, result) => {
    conn.query(`UPDATE customers SET profile_image = '${profile_image}' WHERE id = ${user_id}`, (error, res) => {
        if(error) {
            return result({code: 500, description: error}, null);
        }
        else {
            result(null, {code: 200, result: res});
        }
    });
}

Customer.removeImage = (user_id, result) => {
    conn.query(`UPDATE customers SET profile_image = '' WHERE id = ${user_id}`, (error, res) => {
        if(error) {
            return result({code: 500, description: error}, null);
        }
        else {
            result(null, {code: 200, result: res});
        }
    });
}

Customer.getFeedbacks = (user_id, result) => {
    conn.query(`SELECT counsellor_reviews.id as id, counsellor_reviews.created_at as timestamp, counsellors.name as counsellor_name, counsellors.slug as counsellor_slug, counsellor_reviews.rating as rating, counsellor_reviews.is_recommended as recommendation, counsellor_reviews.title as title, counsellor_reviews.review as review, counsellor_reviews.reply as reply, counsellor_reviews.improvement as improvement FROM counsellor_reviews LEFT JOIN counsellors ON counsellors.id = counsellor_reviews.counsellor_id WHERE counsellor_reviews.customer_id = ${user_id} ORDER BY counsellor_reviews.created_at DESC`, (error, res) => {
        if(error) {
            return result({code: 500, description: error}, null);
        }
        return result(null, {code: 200, description: res});
    });
}

Customer.logout = (refreshAccessToken, result) => {
    refreshAccessTokens = refreshAccessTokens.filter((token) => token !== refreshAccessToken);

    return result(null, {code: 204, description: "Logout successful"});
}

module.exports = Customer;
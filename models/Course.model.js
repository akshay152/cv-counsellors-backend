const conn = require("../config/db.config");

const Course = function(course) {
    this.id = course.id;
}

Course.get = (result) => {
    conn.query(`SELECT id as value, name as label, name as modified FROM courses WHERE status = 1 GROUP BY short_name`, (error, res) => {
        if(error) {
            return result({code: 500, description: error}, null);
        }
        return result(null, {code: 200, description: res});
    });
}

Course.getForConsultation = (result) => {
    conn.query(`SELECT courses.id, courses.short_name, courses.icon, COUNT(DISTINCT(counsellor_affiliations.counsellor_id)) AS counsellors, category_id FROM courses LEFT JOIN counsellor_affiliations ON counsellor_affiliations.course_short_name = courses.short_name WHERE status = 1 GROUP BY courses.short_name`, (error, res) => {
        if(error) {
            return result({code: 500, description: error}, null);
        }
        return result(null, {code: 200, description: res});
    });
}

Course.getDetails = (course_id, result) => {
    conn.query(`SELECT * FROM courses WHERE ID = ${course_id}`, (error, res) => {
        if(error) {
            return result({code: 500, description: error}, null);
        }
        return result(null, {code: 200, description: res});
    });
}

module.exports = Course;
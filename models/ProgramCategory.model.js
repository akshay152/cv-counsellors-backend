const conn = require("../config/db.config");

const ProgramCategory = function(course) {
    this.id = course.id;
}

ProgramCategory.get = (result) => {
    conn.query(`SELECT id, short_name FROM program_categories WHERE status = 1`, (error, res) => {
        if(error) {
            return result({code: 500, description: error}, null);
        }
        return result(null, {code: 200, description: res});
    });
}

module.exports = ProgramCategory;
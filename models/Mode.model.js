const conn = require("../config/db.config");

const Mode = function(mode) {
    this.id = mode.id;
}

Mode.getForCourse = (course_short_name, result) => {
    conn.query(`SELECT modes.id, modes.name, courses.id as course_id FROM courses LEFT JOIN modes ON courses.mode_id = modes.id WHERE courses.short_name = '${course_short_name}' AND modes.status = 1`, (error, res) => {
        if(error) {
            return result({code: 500, description: error}, null);
        }
        return result(null, {code: 200, description: res});
    });
}

Mode.getDescription = (mode_id, result) => {
    conn.query(`SELECT description FROM modes WHERE id = '${mode_id}'`, (error, res) => {
        if(error) {
            return result({code: 500, description: error}, null);
        }
        return result(null, {code: 200, description: res});
    });
}

module.exports = Mode;
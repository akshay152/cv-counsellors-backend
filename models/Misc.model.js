const conn = require("../config/db.config");
const { shootEmail, sendTemplateMail } = require("../config/mailer.config");
const axios  = require("axios");
const { sendTemplateSMS } = require("../config/sms.config");

const Misc = function (misc) {
    this.id = misc.id;
}

Misc.submitReport = (customer_id, counsellor_id, reason, description, result) => {
    conn.query(`SELECT name, mobile FROM customers WHERE id = ${customer_id}`, (error, res) => {
        if (error) {
            return result({ code: 500, description: error }, null);
        }
        const customer = res[0];
        conn.query(`SELECT name, mobile FROM counsellors WHERE id = ${counsellor_id}`, (error, res) => {
            if (error) {
                return result({ code: 500, description: error }, null);
            }
            const counsellor = res[0];

            const htmlContent =
                `<html>
                <head></head>
                <body>
                    <h2>COUNSELLOR REPORTED</h2>
                    <p style="margin: 0px"><b>CUSTOMER</b></p>
                    <p style="margin: 0px">${customer.name} (${customer.mobile})</p><br>

                    <p style="margin: 0px"><b>COUNSELLOR</b></p>
                    <p style="margin: 0px">${counsellor.name} (${counsellor.mobile})</p><br>

                    <p style="margin: 0px"><b>REASON</b></p>
                    <p style="margin: 0px">${reason}</p><br>

                    <p style="margin: 0px"><b>DESCRIPTION</b></p>
                    <p style="margin: 0px">${description}</p>
                </body>
            </html>`

            const sender = {
                name: "CV Counsellors",
                email: "no-reply@cvcounsellors.com"
            };

            const to = [
                {
                    name: 'Akshay Sharma',
                    email: 'akshay@edutra.io'
                },
                {
                    name: 'Sarthak Garg',
                    email: 'sarthakgarg070@gmail.com'
                }
                ,
                {
                    name: 'Rohit Gupta',
                    email: 'rohitgupta25@gmail.com'
                }
            ];

            const subject = `A counsellor has been reported on ${Date.now()}`;

            shootEmail(sender, to, subject, htmlContent)
                .then(() => {
                    return result({ code: 200, description: "Report submitted" });
                })
                .catch((error) => {
                    return result({ code: 500, description: error });
                })
        });
    })
}

Misc.buttonClickMail = (userName, userNumber, userMail, cvID, userCourseID, userSpecializationID, result) => {

    conn.query(`SELECT short_name FROM courses WHERE id = ${userCourseID}`, (error, res) => {
        if (error) {
            return result({ code: 500, description: error }, null);
        }
        const course_name = res[0].short_name;
        conn.query(`SELECT short_name FROM specializations WHERE id = ${userSpecializationID}`, (error, res) => {
            if (error) {
                return result({ code: 500, description: error }, null);
            }
            const specialization_name = res[0].short_name;

            const htmlContent =
                `<html>
                    <head></head>
                    <body>
                        <h2>A STUDENT HAS ASKED FOR A CONSULTATION</h2>
                        <p style="margin: 0px"><b>CUSTOMER</b></p>
                        <p style="margin: 0px">${userName} (${userNumber})</p>
                        <p style="margin: 0px">${course_name} (${specialization_name})</p>
                    </body>
                </html>`

            const sender = {
                name: "CV Counsellors",
                email: "no-reply@cvcounsellors.com"
            };

            const to = [
                {
                    name: 'Akshay Sharma',
                    email: 'akshay@edutra.io'
                },
                {
                    name: 'Sarthak Garg',
                    email: 'sarthakgarg070@gmail.com'
                },
                {
                    name: 'Rohit Gupta',
                    email: 'rohitgupta25@gmail.com'
                }
            ];

            const subject = `A student has asked for a consultation on ${Date.now()}`;
            shootEmail(sender, to, subject, htmlContent);

            const welcomeMailTo = [
                {
                    name: userName,
                    email: userMail
                }
            ];

            const params = {
                id: userNumber,
                password: cvID
            }
            axios.get(`https://api.millionverifier.com/api/v3/?api=41Qb2yB60xBox8hMqfZQjGy1h&email=${userMail}&timeout=10`)
            .then((response) => {
                if(response.data.result === "ok") {
                    sendTemplateMail('WELCOME-MAIL', welcomeMailTo, params);
                }
            });
            sendTemplateSMS('WELCOME-SMS', userNumber, encodeURI(`Thank you for joining the community of college Vidya! 85461+ Students have benefited from Counseling before Selecting Right University. Link: https://cvcounselor.com User Id: ${userNumber} Password: ${cvID}`));
            return result({ code: 200, description: "Emails Sent!" });
        });
    });
}

module.exports = Misc;


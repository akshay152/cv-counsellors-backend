const conn = require("../config/db.config");

const State = function(state) {
    this.id = state.id;
}

State.getByCountry = (countryCode, result) => {
    conn.query(`SELECT id as value, name as label, name as modified FROM states WHERE country_code = '${countryCode}'`, (error, res) => {
        if(error) {
            return result({code: 500, description: error}, null);
        }
        return result(null, {code: 200, description: res});
    });
}

module.exports = State;
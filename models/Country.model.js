const conn = require("../config/db.config");

const Country = function(state) {
    this.id = state.id;
}

Country.getCountryCode = (result) => {
    conn.query(`SELECT id as country_id, name as country_name, timezones as country_timezones, phonecode as value, iso2 as modified, CONCAT('+', phonecode, ' ', '(', iso3, ')') as label FROM countries`, (error, res) => {
        if(error) {
            return result({code: 500, description: error}, null);
        }
        return result(null, {code: 200, description: res});
    });
}

module.exports = Country;
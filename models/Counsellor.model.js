const conn = require("../config/db.config");
const axios = require("axios");
const { sendTemplateMail } = require("../config/mailer.config");
const { sendTemplateSMS } = require("../config/sms.config");

const Counsellor = function (counsellor) {
    this.id = counsellor.id;
}

Counsellor.getDetails = (counsellor_slug, result) => {
    conn.query(`SELECT counsellors.id as id, counsellors.profile_image as image, counsellors.name as name, counsellors.designation as designation, counsellors.qualification as qualification, counsellors.about as about, counsellors.experience as experience, AVG(counsellor_reviews.rating) as avg_rating, counsellors.is_verified as verified, countries.name as locality, counsellors.fees as fees, counsellors.slot_time as slot_time, counsellors.mobile as phone, counsellors.email as email, counsellors.timings as timings, (SELECT CONCAT('[', GROUP_CONCAT(JSON_OBJECT('id', counsellor_affiliations.id, 'name', counsellor_affiliations.course_short_name, 'mode', modes.name)), ']') FROM counsellor_affiliations LEFT JOIN modes ON modes.id = counsellor_affiliations.mode_id WHERE counsellor_affiliations.counsellor_id = counsellors.id) as specializations FROM counsellors LEFT JOIN countries ON countries.id = counsellors.country_id LEFT JOIN counsellor_affiliations ON counsellor_affiliations.counsellor_id = counsellors.id LEFT JOIN counsellor_reviews ON counsellor_reviews.counsellor_id = counsellors.id WHERE counsellors.slug = '${counsellor_slug}'`, (error, res) => {
        if (error) {
            return result({ code: 500, description: error }, null);
        }
        if (res.length === 0) {
            return result({ code: 404, description: "No such user" }, null);
        }
        return result(null, { code: 200, description: res });
    });
}

Counsellor.getDetailsByID = (counsellor_id, result) => {
    conn.query(`SELECT counsellors.id as id, counsellors.profile_image as image, counsellors.slug as slug, counsellors.name as name, counsellors.designation as designation, counsellors.qualification as qualification, counsellors.about as about, counsellors.experience as experience, AVG(counsellor_reviews.rating) as avg_rating, counsellors.is_verified as verified FROM counsellors LEFT JOIN counsellor_reviews ON counsellor_reviews.counsellor_id = counsellors.id WHERE counsellors.id = ${counsellor_id}`, (error, res) => {
        if (error) {
            return result({ code: 500, description: error }, null);
        }
        return result(null, { code: 200, description: res });
    });
}

Counsellor.createReview = (counsellor_id, customer_id, rating, recommendation, title, review, improvement, result) => {
    conn.query(`SELECT id FROM consultations WHERE counsellor_id = ${counsellor_id} AND customer_id = ${customer_id}`, (error, res) => {
        if (error) {
            return result({ code: 500, description: error }, null);
        }
        let verified = 0;
        if (res.length > 0) {
            verified = 1;
        }
        conn.query(`INSERT INTO counsellor_reviews (counsellor_id, customer_id, rating, is_recommended, title, review, improvement, is_verified) VALUES (${counsellor_id}, ${customer_id}, ${rating}, '${recommendation}', '${title}', '${review}', '${improvement}', ${verified})`, (error, res) => {
            if (error) {
                return result({ code: 500, description: error }, null);
            }
            conn.query(`SELECT name, email, mobile FROM customers WHERE id = ${customer_id}`, (error, res) => {
                if (error) {
                    return result(null, { code: 200, description: res });
                }
                else {

                    const customer_name = res[0].name;
                    const customer_email = res[0].email;
                    const customer_mobile = res[0].mobile;

                    const to = [
                        {
                            name: customer_name,
                            email: customer_email
                        }
                    ];

                    const params = {
                        id: customer_id,
                    }

                    axios.get(`https://api.millionverifier.com/api/v3/?api=41Qb2yB60xBox8hMqfZQjGy1h&email=${customer_email}&timeout=10`)
                        .then((response) => {
                            if (response.data.result === "ok") {
                                sendTemplateMail("THANK-YOU-FOR-FEEDBACK", to, params);
                            }
                        });

                    sendTemplateSMS("THANK-YOU-FOR-FEEDBACK-SMS", customer_mobile, encodeURI(`Thanks for your valuable feedback. We are on a mission to make our guidance even more student-friendly! College Vidya- Your right university guide!`));
                    return result(null, { code: 200, description: res });
                }
            })
        });
    });
}

Counsellor.updateReview = (feedbackID, rating, title, review, result) => {
    conn.query(`UPDATE counsellor_reviews SET rating = ${rating}, title = '${title}', review = '${review}' WHERE id = ${feedbackID}`, (error, res) => {
        if (error) {
            return result({ code: 500, description: error }, null);
        }
        return result(null, { code: 200, description: res });
    });
}

Counsellor.deleteReview = (feedbackID, result) => {
    conn.query(`DELETE FROM counsellor_reviews WHERE id = ${feedbackID}`, (error, res) => {
        if (error) {
            return result({ code: 500, description: error }, null);
        }
        return result(null, { code: 200, description: res });
    });
}

Counsellor.getConsultations = (counsellor_slug, result) => {
    conn.query(`SELECT consultations.start_time as start_time FROM consultations LEFT JOIN counsellors ON counsellors.id = consultations.counsellor_id WHERE counsellors.slug = '${counsellor_slug}'`, (error, res) => {
        if (error) {
            return result({ code: 500, description: error }, null);
        }
        return result(null, { code: 200, description: res });
    });
}

Counsellor.getCourses = (counsellor_id, result) => {
    conn.query(`SELECT courses.id as id, courses.short_name as name FROM counsellor_affiliations LEFT JOIN courses ON courses.short_name = counsellor_affiliations.course_short_name WHERE counsellor_affiliations.counsellor_id = ${counsellor_id} GROUP BY courses.short_name`, (error, res) => {
        if (error) {
            return result({ code: 500, description: error }, null);
        }
        return result(null, { code: 200, description: res });
    });
}

Counsellor.getCounsellorModesForACourse = (counsellor_id, course_short_name, result) => {
    conn.query(`SELECT modes.id, modes.name, modes.description FROM counsellor_affiliations LEFT JOIN modes ON modes.id = counsellor_affiliations.mode_id WHERE counsellor_affiliations.counsellor_id = ${counsellor_id} AND counsellor_affiliations.course_short_name = '${course_short_name}'`, (error, res) => {
        if (error) {
            return result({ code: 500, description: error }, null);
        }
        return result(null, { code: 200, description: res });
    });
}

Counsellor.getCounsellorSpecializationsForModeCourse = (course_short_name, mode_id, result) => {
    conn.query(`SELECT id FROM courses WHERE short_name = '${course_short_name}' AND mode_id = ${mode_id}`, (error, res) => {
        if (error) {
            return result({ code: 500, description: error }, null);
        }
        if (res.length > 0) {
            conn.query(`SELECT id, name from specializations WHERE course_id = ${res[0].id}`, (error, res) => {
                if (error) {
                    return result({ code: 500, description: error }, null);
                }
                return result(null, { code: 200, description: res });
            })
        }
        else {
            return result(null, { code: 200, description: res });
        }
    });
}

Counsellor.getFromFilter = (course_short_name, mode_id, genders, sort_method, result) => {

    var genderFilter = '';

    if (genders !== '') {
        genders = '\'' + genders.split(',').join('\',\'') + '\'';
        genderFilter = `AND counsellors.gender IN (${genders})`;
    }

    var sortFilter = '';

    if (sort_method === 'price-low') {
        sortFilter = 'ORDER BY counsellors.fees ASC';
    }

    if (sort_method === 'price-high') {
        sortFilter = 'ORDER BY counsellors.fees DESC';
    }

    if (sort_method === 'experience') {
        sortFilter = 'ORDER BY counsellors.experience DESC';
    }

    if (sort_method === 'review') {
        sortFilter = 'ORDER BY counsellor_reviews.rating DESC';
    }

    var query = `SELECT counsellors.id, counsellors.profile_image as image, CONCAT('/counsellor/', counsellors.slug) as profile_link, counsellors.qualification as qualification, counsellors.is_verified as verified, counsellors.name, counsellors.experience, counsellors.fees, (SELECT CONCAT('[', GROUP_CONCAT(JSON_OBJECT('name', counsellor_affiliations.course_short_name, 'mode', modes.name, 'mode_id', modes.id)), ']') FROM counsellor_affiliations LEFT JOIN modes ON modes.id = counsellor_affiliations.mode_id WHERE counsellor_affiliations.counsellor_id = counsellors.id) as specializations, (SELECT CAST(JSON_OBJECT('count', COUNT(counsellor_reviews.id), 'rating', CAST(AVG(counsellor_reviews.rating) AS DECIMAL(10, 1))) AS CHAR) FROM counsellor_reviews WHERE counsellor_reviews.counsellor_id = counsellors.id) as reviews FROM counsellors LEFT JOIN counsellor_affiliations ON counsellor_affiliations.counsellor_id = counsellors.id LEFT JOIN counsellor_reviews ON counsellor_reviews.counsellor_id = counsellors.id WHERE counsellor_affiliations.course_short_name = '${course_short_name}' ${genderFilter} AND counsellor_affiliations.mode_id = '${mode_id}' GROUP BY counsellors.id ${sortFilter}`;

    conn.query(query, (error, res) => {
        if (error) {
            return result({ code: 500, description: error }, null);
        }
        return result(null, { code: 200, description: res });
    })
}

module.exports = Counsellor;
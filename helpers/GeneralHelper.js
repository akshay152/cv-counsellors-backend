const jwt = require("jsonwebtoken");

module.exports = {
    isEmptyObject: function (obj) {
        return !Object.keys(obj).length;
    },
    
    mysql_real_escape_string: function (str) {
        // eslint-disable-next-line no-control-regex
        return str.replace(/[\0\x08\x09\x1a\n\r"'\\%]/g, function (char) {
            switch (char) {
                case "\0":
                    return "\\0";
                case "\x08":
                    return "\\b";
                case "\x09":
                    return "\\t";
                case "\x1a":
                    return "\\z";
                case "\n":
                    return "\\n";
                case "\r":
                    return "\\r";
                case "\"":
                case "'":
                case "\\":
                case "%":
                    return "\\"+char;
                default:
                    return ;
            }
        });
    },

    generateAccessToken: function(user) {
        return jwt.sign({id: user.id}, process.env.ACCESS_TOKEN_SECRET, {
            expiresIn: "7d",
        });
    },

    generateRefreshAccessToken: function(user) {
        return jwt.sign({id: user.id}, process.env.REFRESH_ACCESS_TOKEN_SECRET);
    },

    authenticateToken: function(req, res, next) {
        const authHeader = req.headers['authorization'];
        const token = authHeader && authHeader.split(" ")[1];

        if(token == null)
        return res.status(401).send({
            "error": "No Access Token.",
            "detailedmessage": "No access token is provided. Please verify the token is correct and provided as a bearer token in the authorization header."
        });

        jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
            if(err)
            return res.status(401).send({
                "error": "Invalid Access Token.",
                "detailedmessage": "The access token provided is not valid. Please verify the token is correct and provided as a bearer token in the authorization header."
            });
            req.user = user
            next()
        })
    }
}
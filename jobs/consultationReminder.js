const moment = require("moment");
const { sendTemplateMail } = require("../config/mailer.config");
const axios = require("axios");
const mysql = require("mysql");
const { sendTemplateSMS } = require("../config/sms.config");
require("../config/db.config");

const conn = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "cv_counsellors"
});

conn.connect(error => {
    if (error) throw error;
    console.log("Successfully connected to the database.");
});

const ConsultationReminderJob = () => {
    conn.query(`SELECT id, start_time FROM consultations WHERE TIMESTAMPDIFF(MINUTE, '${moment(new Date()).format('YYYY-MM-DDTHH:mm')}', start_time) = 30 AND finished = 0`, (error, consultations) => {
        if (error) {
            return;
        }
        if (consultations.length > 0) {
            consultations.forEach(consultation => {
                conn.query(`SELECT customers.mobile as customer_mobile, customers.cv_id as customer_password, customers.name as customer_name , customers.email as customer_email, counsellors.name as counsellor_name, counsellors.email as counsellor_email FROM consultations LEFT JOIN customers ON customers.id = consultations.customer_id LEFT JOIN counsellors ON counsellors.id = consultations.counsellor_id WHERE consultations.id = ${consultation.id}`, (error, res) => {
                    if (error) {
                        return;
                    }

                    const customerName = res[0].customer_name;
                    const customerEmail = res[0].customer_email;
                    const customerMobile = res[0].customer_mobile;
                    const customerPassword = res[0].customer_password;
                    const counsellorName = res[0].counsellor_name;
                    const counsellorEmail = res[0].counsellor_email;

                    const toCustomer = [
                        {
                            name: customerName,
                            email: customerEmail
                        }
                    ];

                    const customerParams = {
                        date: moment(consultation.start_time).format("LL"),
                        time: moment(consultation.start_time).format("LT"),
                        id: customerMobile,
                        password: customerPassword,
                        link: "https://cvcounselor.com/user/consultations?consultation_id=" + consultation.id
                    };

                    axios.get(`https://api.millionverifier.com/api/v3/?api=41Qb2yB60xBox8hMqfZQjGy1h&email=${customerEmail}&timeout=10`)
                        .then((response) => {
                            if (response.data.result === "ok") {
                                sendTemplateMail("CUSTOMER-COUNSELLING-REMINDER", toCustomer, customerParams);
                                sendTemplateSMS("CUSTOMER-COUNSELLING-REMINDER-SMS", customerMobile, encodeURI(`Hey, this is your reminder that your free consultation is about to begin in 30 minutes with a College Vidya mentor. So, be on time! ${customerParams.date} ${customerParams.time}`))
                            }
                        });

                    const toCounsellor = [
                        {
                            name: counsellorName,
                            email: counsellorEmail
                        }
                    ];

                    const params = {
                        link: "https://partner.cvcounselor.com/dashboard/consultations?consultation_id=" + consultation.id
                    };

                    axios.get(`https://api.millionverifier.com/api/v3/?api=41Qb2yB60xBox8hMqfZQjGy1h&email=${counsellorEmail}&timeout=10`)
                        .then((response) => {
                            if (response.data.result === "ok") {
                                sendTemplateMail("COUNSELLOR-COUNSELLING-REMINDER", toCounsellor, params);
                            }
                        });
                });
            });
        }
    })
}

module.exports = ConsultationReminderJob;
const scheduler = require('node-schedule');
const ConsultationReminderJob = require('./consultationReminder');

scheduler.scheduleJob('Counselling Reminder Job', "* * * * *", ConsultationReminderJob);
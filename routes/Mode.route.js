module.exports = app => {
    const mode = require("../controllers/Mode.controller");

    //Get mode
    app.get("/mode/:course_short_name", mode.getForCourse);

    //Get mode description
    app.get("/mode/description/:mode_id", mode.getDescription);
}
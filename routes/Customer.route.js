module.exports = app => {
    const customer = require("../controllers/Customer.controller");
    const generalHelper = require("../helpers/GeneralHelper");

    //Refesh Tokens
    app.post("/customer/token/refresh", customer.refreshAccessTokens);

    //Create
    app.post("/customer/create", customer.create);

    //Login
    app.post("/customer/login", customer.login);

    //Get details
    app.get("/customer/details/:user_id", customer.details);

    //Update profile
    app.put("/customer/update/:user_id", generalHelper.authenticateToken, customer.update);

    //Update profile image
    app.put("/customer/profile-image/:user_id", generalHelper.authenticateToken, customer.updateImage);

    //Remove profile image
    app.put("/customer/profile-image/delete/:user_id", generalHelper.authenticateToken, customer.removeImage);

    //Update email
    app.post("/customer/update-email", generalHelper.authenticateToken, customer.updateEmail);

    //Verify Email
    app.get("/customer/verify-email", customer.verifyEmail);

    //Get feedbacks
    app.get("/customer/feedbacks/:user_id", generalHelper.authenticateToken, customer.getFeedbacks);

    //Logout
    app.post("/customer/logout", customer.logout);
}
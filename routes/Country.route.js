module.exports = app => {
    const country = require("../controllers/Country.controller");

    //Get country code and phone code
    app.get("/country", country.getCountryCode);
}
module.exports = app => {
    const course = require("../controllers/Course.controller");

    //Get courses
    app.get("/course", course.get);

    //Get courses for consultation
    app.get("/course-consultation", course.getForConsultation);

    //Get course details
    app.get("/course/details/:course_id", course.getDetails);
}
module.exports = app => {
    const programCategory = require("../controllers/ProgramCategory.controller");

    //Get courses
    app.get("/program-category", programCategory.get);
}
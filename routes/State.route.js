module.exports = app => {
    const state = require("../controllers/State.controller");

    //Get states by country code
    app.get("/state", state.getByCountry);
}
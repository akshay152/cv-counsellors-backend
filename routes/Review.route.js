module.exports = app => {
    const review = require("../controllers/Review.controller");

    //Get reviews for a counsellor
    app.get("/review/:counsellor_slug", review.getForCounsellor);
}
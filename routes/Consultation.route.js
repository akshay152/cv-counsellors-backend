module.exports = app => {
    const consultation = require("../controllers/Consultation.controller");
    const generalHelper = require("../helpers/GeneralHelper");

    //Create a new consultation
    app.post("/consultation/create", generalHelper.authenticateToken, consultation.create);

    //Verify consulation
    app.get("/consulation/live/verify/now", consultation.verify);

    //Get consultations for a customer
    app.get("/consultation/:customer_id", generalHelper.authenticateToken, consultation.getForACustomer);

    //Get messages for a consultation
    app.get("/consultation/:customer_id/:consultation_id", generalHelper.authenticateToken, consultation.getMessages);

    //Add a message to a consultation
    app.post("/consultation/message", generalHelper.authenticateToken, consultation.addMessage);

    //Read counsellor messages for a consultation
    app.put("/consultation/read", generalHelper.authenticateToken, consultation.read);

    //Reopen a consultation
    app.put("/consultation/reopen", generalHelper.authenticateToken, consultation.reopen);

    //Add customer busy message to consultation
    app.post("/consultation/busy/system/insert", generalHelper.authenticateToken, consultation.addBusyCallDeclined);
}
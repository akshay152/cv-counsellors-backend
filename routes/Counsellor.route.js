const generalHelper = require("../helpers/GeneralHelper");

module.exports = app => {
    const counsellor = require("../controllers/Counsellor.controller");

    //Get details for a counsellor
    app.get("/counsellor/:counsellor_slug", counsellor.getDetails);

    //Get details for a counsellor by id
    app.get("/counsellor/details/:counsellor_id", counsellor.getDetailsByID);

    //Create review for a counsellor
    app.post("/counsellor/review/create", generalHelper.authenticateToken, counsellor.createReview);

    //Update review for a counsellor
    app.put("/counsellor/review/update", generalHelper.authenticateToken, counsellor.updateReview);

    //Delete review for a counsellor
    app.delete("/counsellor/review/delete", generalHelper.authenticateToken, counsellor.deleteReview);

    //Get consultations for a counsellor
    app.get("/counsellor/consultations/:counsellor_slug", counsellor.getConsultations);

    //Get courses for a counsellor
    app.get("/counsellor/courses/:counsellor_id", counsellor.getCourses);

    //Get modes for a course for a counsellor
    app.get("/counsellor/modes/:counsellor_id/:course_short_name", counsellor.getCounsellorModesForACourse);

    //Get specialization for a course, a mode for a counsellor
    app.get("/counsellor/specializations/:course_short_name/:mode_id", counsellor.getCounsellorSpecializationsForModeCourse);

    //post counsellors from filter
    app.post("/counsellor/get-from-filter", counsellor.getFromFilter);
}
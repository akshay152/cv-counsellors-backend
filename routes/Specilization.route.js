module.exports = app => {
    const specialization = require("../controllers/Specialization.controller");

    //Get for mode and course
    app.get("/specialization", specialization.getForCourseAndMode);
}
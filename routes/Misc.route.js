module.exports = app => {
    const misc = require("../controllers/Misc.controller");
    const generalHelper = require("../helpers/GeneralHelper");

    //Submit report
    app.post("/misc/report/new", generalHelper.authenticateToken, misc.submitReport);

    //Button click mail
    app.post("/misc/button-click-mail", generalHelper.authenticateToken, misc.buttonClickMail);
}
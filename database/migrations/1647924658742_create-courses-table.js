module.exports = {
    "up": `CREATE TABLE courses (
        id int NOT NULL AUTO_INCREMENT,
        name varchar(255) NOT NULL,
        short_name varchar(255) NOT NULL,
        mode_id int,
        status tinyint(1) DEFAULT 1 NOT NULL,
        updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (id),
        FOREIGN KEY (mode_id) REFERENCES modes(id)
    )`,
    "down": "DROP TABLE courses"
}
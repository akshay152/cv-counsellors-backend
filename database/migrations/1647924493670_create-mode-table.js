module.exports = {
    "up": `CREATE TABLE modes (
        id int NOT NULL AUTO_INCREMENT,
        name varchar(255) NOT NULL,
        status tinyint(1) DEFAULT 1 NOT NULL,
        updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (id)
    )`,
    "down": "DROP TABLE modes"
}
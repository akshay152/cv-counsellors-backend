module.exports = {
    "up": `CREATE TABLE customers (
            id int NOT NULL AUTO_INCREMENT, 
            name varchar(255) NOT NULL,
            mobile varchar(20) NOT NULL,
            secondary_mobile varchar(20) NULL,
            password varchar(255) NOT NULL,
            email varchar(255) NULL,
            profile_image varchar(255) NOT NULL,
            gender varchar(10) NULL,
            dob varchar(15) NULL,
            timezone varchar(100) NOT NULL,
            house_number varchar(100) NULL,
            colony_locality varchar(100) NULL,
            city varchar(100) NULL,
            state varchar(100) NULL,
            country varchar(100) NULL,
            pincode varchar(10) NULL,
            language varchar(10) NULL,
            status tinyint(1) DEFAULT 1 NOT NULL,
            updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (id)
        )`,
    "down": "DROP TABLE customers"
}